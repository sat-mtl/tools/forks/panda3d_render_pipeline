"""

RenderPipeline

Copyright (c) 2017 Emmanuel Durand <emmanueldurand@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""

from rpcore.render_stage import RenderStage


class ImmersiveStage(RenderStage):

    """ This stage handles the immersive rendering """

    def __init__(self, pipeline):
        RenderStage.__init__(self, pipeline)
        self.pipeline = pipeline

    @property
    def produced_defines(self):
        return {"IM_ACTIVE": self.pipeline.settings["immersive.active"],
                "IM_INWARD": self.pipeline.settings["immersive.inward"],
                "IM_INWARD_SCALE": self.pipeline.settings["immersive.inward_scale"]}

    def create(self):
        pass
